# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Oliver Muhammad Fadhlurrahman - 1806241154

### Heroku Link
http://adpro-final-oliver.herokuapp.com/

### Config
API key in application.properties is a configuration and needs to be set as an environment variable. To fix
this, we have add new environment variable named in IntelliJ TEXT_ANALYTICS_API_KEY and assign the value
the same as on the given application.properties file.

### Logging
In logging, we have to treat logs as event stream. Log provides the behaviour of running app that can be
seen in ScienceDirectPaperFinder.java. We have to change System.out.println to log.error and add @Log4j2
annotation to it.

## Task 2 Result (Prometheus)
![count](images/task2.png)

